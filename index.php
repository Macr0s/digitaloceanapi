<?php
	require_once("util/CCurl.load.php");
	require_once("util/AbstractCredential.php");

	namespace Guru\Mfilippi\DigitalOcean;

	use Guru\Mfilippi\DigitalOcean\Util\AbstractCredential;
	use Guru\Mfilippi\DigitalOcean\Util\CCurl;
	use Guru\Mfilippi\DigitalOcean\Util\ActionEndpoint;

	class DigitalOcean{
		private $credential;
		private $curl;

		public __construct(AbstractCredential $c){
			$this->credential = $c;
			$this->curl = new CCurl();
			$this->curl->addOption(CURLOPT_HTTPHEADER, array(
		    	"Authorization: Bearer ". $c->Bearer()
		    ));
		}

		public function actions(){
			$t = new ActionEndpoint();
			$t->setCurl($this->curl);
			return $t;
		}
	}
?>
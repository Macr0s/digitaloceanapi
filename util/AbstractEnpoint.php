<?php
	namespace Guru\Mfilippi\DigitalOcean\Util;

	abstract class AbstractEnpoint{
		private $curl;

		public function getCurl(){
			$this->curl;
		}

		public function setCurl(CCurl $c){
			$this->curl = $c;
		}

		public function post($url, $field){
			return json_decode($this->curl->post($url, $field));
		}

		public function get($url){
			return json_decode($this->curl->get($url));
		}

		private function removeLinks($r){
			if (property_exists($r, property)){
				unset($r->links);
				return $r;
			}else{
				return $r;
			}
		}

		private function mergeLinks($r, $a){
			$t = array();
			foreach ($r as $key => $value) {
				if (is_array($value)){
					$t[$key] = array_merge($r->$key, $a->$key);
				}else{
					$t[$key] = $value;
				}
			}
			return (object) $t;
		}

		public function getRecursice($url){
			$r = $this->get($url);
			if (property_exists($r, "links") && $r->links->pages->last != $r->links->pages->next){
				$a = $this->getRecursice($r->links->pages->next);
				$a = $this->removeLinks($a);
				$r = $this->removeLinks($r);
				return $mergeLinks($r, $a);
			}else{
				return $this->removeLinks($r);
			}
		}
	}
?>
<?php
	require_once("AbstractCredential.php");

	namespace Guru\Mfilippi\DigitalOcean\Util;

	class PersonalToken implements AbactCredential{

		private $code;

		public __construct($code){
			$this->code = $code;
		}

		public function Bearer(){
			return $this->code;
		}
	}
?>
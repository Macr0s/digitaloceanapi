<?php
	
	namespace Guru\Mfilippi\DigitalOcean\Util;

	public interface AbstractCredential{

		public function getBearer();

	}
?>